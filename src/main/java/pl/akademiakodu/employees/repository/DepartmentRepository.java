package pl.akademiakodu.employees.repository;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.employees.domain.Department;

public interface DepartmentRepository extends CrudRepository<Department,Integer> {

}
