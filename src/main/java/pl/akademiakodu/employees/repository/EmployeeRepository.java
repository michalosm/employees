package pl.akademiakodu.employees.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.employees.domain.Employee;


public interface EmployeeRepository extends CrudRepository<Employee,Integer> {

    @Query("SELECT e FROM Employee e WHERE e.job = ?1")
    Iterable<Employee> findEmployeesByJob(String  job);

}
