package pl.akademiakodu.employees.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "DEPT")
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DEPTNO")
    private Integer deptno;

    @Column(name = "DNAME")
    private String dname;

    @Column(name = "LOC")
    private String loc;

    @OneToMany(mappedBy = "department")
    private List<Employee> employeeList = new ArrayList<>();

}
