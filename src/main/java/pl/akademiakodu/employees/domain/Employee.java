package pl.akademiakodu.employees.domain;


import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;


@NamedQuery(name="Employee.findAll", query="SELECT e FROM Employee e")
@Data
@Table(name = "EMP")
@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer EMPNO;

    @Column(name = "ENAME")
    private String ename;

    @Column(name = "MGR")
    private Integer mgr;

    @Column(name = "HIREDATE")
    private LocalDate hiredate;

    @Column(name = "SAL")
    private Double salary;

    @Column(name = "COMM")
    private Double commision;

    @Column(name = "JOB")
    private String job;

    @ManyToOne
    @JoinColumn(name = "DEPTNO")
    private Department department;

}
