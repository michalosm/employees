package pl.akademiakodu.employees.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.akademiakodu.employees.domain.Department;
import pl.akademiakodu.employees.repository.DepartmentRepository;

@RestController
@RequestMapping("/")
public class DepartmentController {

    private DepartmentRepository departmentRepository;

    public DepartmentController(DepartmentRepository departmentRepository){
        this.departmentRepository = departmentRepository;
    }

    @GetMapping("departments")
    public Iterable<Department> index(){
        return departmentRepository.findAll();
    }
}
