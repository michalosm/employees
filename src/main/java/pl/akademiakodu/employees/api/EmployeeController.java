package pl.akademiakodu.employees.api;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.akademiakodu.employees.domain.Employee;
import pl.akademiakodu.employees.repository.EmployeeRepository;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class EmployeeController {

    private EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    @GetMapping("employees")
    public Iterable<Employee> index(){
        return employeeRepository.findAll();
    }

    @GetMapping("employees/list")
    public Iterable<Employee> findEmployeesByJob(@RequestParam String job){
        return employeeRepository.findEmployeesByJob(job);
    }


}
